(function() {
    angular.module('App')
        .controller('ListUserCtrl', [MyKendo]);

    function MyKendo() {
        function onChange(e) {
            var rows = e.sender.select();
            rows.each(function(e) {
                var grid = $("#grid").data("kendoGrid");
                var dataItem = grid.dataItem(this);
                console.log(dataItem.Info);
                $("#window").kendoWindow({
                    height: 65,
                    width: 300,
                    position: {
                        top: 200,
                        left: "42.5%"
                    },
                    title: "User Information"
                });
                var myWindow = $("#window").data("kendoWindow");
                myWindow.open();
                myWindow.content(dataItem.Info);
            })
        }

        $("#grid").kendoGrid({
            dataSource: {
                type: "json",
                data: arr,
                pageSize: 2,
                schema: {
                    model: {
                        fields: {
                            FirstName: {
                                type: "string"
                            },
                            LastName: {
                                type: "string"
                            },
                            Email: {
                                type: "string"
                            },
                            Password: {
                                type: "string"
                            }
                        }
                    }
                },
            },
            change: onChange,
            selectable: "row",
            columns: [{
                field: "FirstName",
                title: "First Name",
                width: "25%"
            }, {
                field: "LastName",
                title: "Last Name",
                width: "25%"
            }, {
                field: "Email",
                title: "Email",
                width: "25%"
            }, {
                field: "Password",
                title: "Password",
                width: "25%"
            }],
            scrollable: false,
            pageable: {
                pageSizes: [1, 2, 3, 4, 5]
            }
        })

        document.querySelectorAll('.k-pager-numbers-wrap').forEach(function(a) {
            a.remove()
        })
    }
})();