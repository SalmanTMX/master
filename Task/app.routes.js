(function() {
    angular.module("App").config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('Home', {
                url: "/",
                templateUrl: "app/home/home.html"
            })

        .state('Add User', {
            url: "/Add",
            templateUrl: "app/add-user/add.user.html",
            controller: 'AddUserCtrl'
        })

        .state('List User', {
            url: "/List",
            templateUrl: "app/list-user/list.user.html",
            controller: 'ListUserCtrl'
        })

        $urlRouterProvider.otherwise('/');
    });
})();